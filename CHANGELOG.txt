LoginToboggan Reminder CHANGELOG
================================
richard@peacocksoftware.com


2016-07-24
  Made it so message won't try to display if we are going to the validate link itself.

2016-07-17
  Initial release of beta version for Drupal 7
